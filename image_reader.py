import cv2
import glob
import numpy as np
import torch
from torchvision import transforms


INPUT_SIZE = 320
FILES = glob.glob("test_images/*")

TORCH_TRANSFORM = transforms.Compose([
    transforms.Lambda(lambda x: x/(x.reshape(x.size(0), -1).max(dim=-1, keepdim=True)[0])),
    transforms.Lambda(lambda x: x - torch.tensor((0.485, 0.456, 0.406)).reshape(1, 3, 1, 1)),
    transforms.Lambda(lambda x: x / torch.tensor((0.229, 0.224, 0.225)).reshape(1, 3, 1, 1))
])


def load_rgb(path, target_size=(320, 320)):
    img = cv2.imread(path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    if target_size:
        img = cv2.resize(img, target_size)
    return img
    
def get_img(index=None):
    '''
    get image by index
    if index is None, get a random image
    '''
    if index is None:
        choice = np.random.choice(FILES)
    else:
        choice = FILES[index%len(FILES)]
    img = load_rgb(choice, (INPUT_SIZE, INPUT_SIZE))
    return img

def img2batch(img):
    img = torch.Tensor(img).unsqueeze(0).permute(0, 3, 1, 2).float()
    return img

def img2torchInput(img):
    img = img2batch(img)
    return TORCH_TRANSFORM(img)

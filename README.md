## Setup

> pip install -r requirements.txt

## Inference

- CoreML: `infer_coreml.ipynb`
- TF-Lite: `infer_tflite.ipynb`

## Note

- replace the model path
- models file can be obtained from [gdrive](https://drive.google.com/drive/folders/1Lt9nFlQnE_QhEwDDXpqzJCIOzTCv9JtY?usp=sharing)